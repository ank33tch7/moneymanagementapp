This is a ASP.NET 6 Project which is integrated with angular. And this project is still in development phase. 

If you want to run this project then follow these following steps:
1) Clone this project
2) Navigate to ClientApp and install NPM (Node Package Manager)
3) Create a migration in DAL project and update database
4) Run angular project with command "ng s"
5) And Run your asp.net core project. The dot net project will pick up angular project and run automatically.

[Note: This project includes angular project so install node js and angualr cli on your computer.]
